from Parser import Parser
from Brain import Brain
from path import Path
import inspect

PROJ_DIR = Path(inspect.getsourcefile(lambda: 0)).abspath().parent
IN_DIR = PROJ_DIR / 'in'
OUT_DIR = PROJ_DIR / 'out'
OUT_DIR.mkdir_p()

if __name__ == '__main__':
    for f in IN_DIR.files():
        print(f + ' is starting')

        parser = Parser(f)
        parser.parse()

        brain = Brain(parser)
        brain.run()

        print(f + ' is done')
