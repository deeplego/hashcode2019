from Photo import Photo
from datetime import datetime
from path import Path
import inspect

PROJ_DIR = Path(inspect.getsourcefile(lambda: 0)).abspath().parent
IN_DIR = PROJ_DIR / 'in'
OUT_DIR = PROJ_DIR / 'out'
OUT_DIR.mkdir_p()


class Parser:

    def __init__(self, filename):
        self.filename = filename
        self.N = None
        self.list_photos = []

    def parse(self):
        with open(self.filename, 'r') as f:
            self.N = int(f.readline()[:-1])
            assert(1 <= self.N and self.N <= 1e5)
            for i in range(self.N):
                photo_dm: list = f.readline()[:-1].split()
                char_photo = photo_dm[0]
                id_photo = int(photo_dm[1])
                tags_photo = set(photo_dm[2:])
                self.list_photos.append(Photo(i, tags_photo, char_photo))

    def write_output(self, slides):

        with open(OUT_DIR / '{0}_{1}.out'.format(self.filename.stem, datetime.utcnow().strftime('%H%S')), 'w') as f:
            n_slides = len(slides)
            f.write(str(n_slides))
            for i in range(n_slides):
                f.write('\n' + str(slides[i]))


if __name__ == '__main__':
    a_filename = IN_DIR / 'a_example.txt'

    parser = Parser(a_filename)
    parser.parse()
    print(parser.N)
    print(parser.list_photos)

